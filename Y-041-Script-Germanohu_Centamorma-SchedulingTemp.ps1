<# 
.NOTES 
    ***************************************************************************** 
	ETML 
	Name: 	Y-041-Script-SchedulingTemp-Germanohu_Centamorma.ps1 
    Author: Hugo Germano, Marco Centamori
    Date:	29.05.19
 	***************************************************************************** 
    Modifications 
 	Date  : - 
 	Author: - 
 	Reason: - 
 	***************************************************************************** 
.SYNOPSIS 
    Summary  
 	 
.DESCRIPTION 
    Script automatique qui zip le fichier C:\Windows\Temp dans le fichier G:\DossierCommun\DateDuJour
 	 
.EXAMPLE 
    TaskPath                                       TaskName                          State     
--------                                       --------                          -----     
\                                              Nettoyage                         Ready     

PSPath            : Microsoft.PowerShell.Core\FileSystem::G:\DossierCommun\22.05.2019
PSParentPath      : Microsoft.PowerShell.Core\FileSystem::G:\DossierCommun
PSChildName       : 22.05.2019
PSDrive           : G
PSProvider        : Microsoft.PowerShell.Core\FileSystem
PSIsContainer     : True
Name              : 22.05.2019
Parent            : DossierCommun
Exists            : True
Root              : G:\
FullName          : G:\DossierCommun\22.05.2019
Extension         : .2019
CreationTime      : 22.05.2019 15:05:14
CreationTimeUtc   : 22.05.2019 13:05:14
LastAccessTime    : 22.05.2019 15:05:14
LastAccessTimeUtc : 22.05.2019 13:05:14
LastWriteTime     : 22.05.2019 15:05:14
LastWriteTimeUtc  : 22.05.2019 13:05:14
Attributes        : Directory
Mode              : d-----
BaseName          : 22.05.2019
Target            : {}
LinkType          :
#> 
param([string]$path)
    #Test si le script se lance en administrateur
    if(([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")){ 
        #Test voir si la t�che Nettoyage existe
        if(Get-ScheduledTask Nettoyage -ErrorAction Ignore){}  
        else
        {
        #instancie une variable contenant l'action d'une t�che journali�re 
        $task = New-ScheduledTaskAction -Execute 'Powershell.exe' -Argument "$PSCommandPath $path"; 
        #instancie une variable contenant le d�clenchement d'une t�che journali�re         
        $trigger = New-ScheduledTaskTrigger -Daily -At 22:00pm; 
        #instancie une variable contenant le param�tre d'une t�che journali�re         
        $settings = New-ScheduledTaskSettingsSet -MultipleInstances Parallel;
        #cr�� une t�che journali�re
        Register-ScheduledTask -Action $task -Trigger $trigger -TaskName "Nettoyage" -Description "Nettoie le temp" -RunLevel Highest -Force; 
        }
        #v�rification du chemin pour le fichier de sauvegarde et cr�ation du dernier s'il n'existe pas
        if((Test-Path "$path\DossierCommun") -eq $false){
          New-Item -Path $path\DossierCommun -ItemType Directory;
        }
        #Obtient la date du jour
        $Date = Get-Date -Format "dd.MM.yyyy";
        #Test si le fichier "G:\DossierCommun\$Date" existe
        if((Test-Path "$path\DossierCommun\$Date") -eq $false)
        {
        #v�rification du chemin de log et c�ration de celui ci s'il n'existe pas
          if((Test-Path "$path\DossierCommun\log") -eq $false){
            New-Item -Path $path\DossierCommun\log -ItemType Directory;
          }
          #cr�� un nouveau dossier
          New-Item -Path $path\DossierCommun\$Date -ItemType Directory;
          #Archive le C:\Windows\temp dans le fichier cr�� avant
          Compress-Archive -Path C:\Windows\Temp -DestinationPath $path\DossierCommun\$Date\Temp -Force;
          #variable pour le log
          $size = (get-item $path\DossierCommun\$Date\Temp.zip).Length
          $log = "$Date $env:UserName $size"+"Ko : R�ussite";
          Write-Output $log >> $path\DossierCommun\log\log.txt;
        }
    } 
    else
    { 
        echo "Veuillez lancer le script avec les droits administrateur"; 
    }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                